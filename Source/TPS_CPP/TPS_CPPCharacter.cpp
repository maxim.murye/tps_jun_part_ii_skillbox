// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_CPPCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATPS_CPPCharacter::ATPS_CPPCharacter()
		//		: AxisX(0.0f), AxisY(0.0f)
{
				// Set size for player capsule
				GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

				// Don't rotate character to camera direction
				bUseControllerRotationPitch = false;
				bUseControllerRotationYaw = false;
				bUseControllerRotationRoll = false;

				// Configure character movement
				GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
				GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
				GetCharacterMovement()->bConstrainToPlane = true;
				GetCharacterMovement()->bSnapToPlaneAtStart = true;

				// Create a camera boom...
				CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
				//CameraBoom->SetupAttachment(RootComponent);

				CameraBoom->SetupAttachment(GetMesh(), "CameraSocket");

				CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
				CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
				CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

				// Create a camera...
				TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
				TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
				TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

				// Create a decal in the world to show the cursor's location
				CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
				CursorToWorld->SetupAttachment(RootComponent);
				static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
				if (DecalMaterialAsset.Succeeded())
				{
					CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
				}
				CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
				CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

				// Activate ticking in order to update the cursor every frame.
				PrimaryActorTick.bCanEverTick = true;
				PrimaryActorTick.bStartWithTickEnabled = true;

				/** Camera Zoom*/
				MinZoomLength = 0.f;
				MaxZoomLength = 1000.f;
				DefaultArmLength = 800.f;
				ZoomStep = 10.f;
				CameraBoom->TargetArmLength = DefaultArmLength;
}

void ATPS_CPPCharacter::Tick(float DeltaSeconds)
{
				    Super::Tick(DeltaSeconds);
				
					if (CursorToWorld != nullptr)
					{
								if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
								{
												if (UWorld* World = GetWorld())
												{
																FHitResult HitResult;
																FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
																FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
																FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
																Params.AddIgnoredActor(this);
																World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
																FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
																CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
												}
								}
								else if (APlayerController* PC = Cast<APlayerController>(GetController()))
										{
												FHitResult TraceHitResult;
												PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
												FVector CursorFV = TraceHitResult.ImpactNormal;
												FRotator CursorR = CursorFV.Rotation();
												CursorToWorld->SetWorldLocation(TraceHitResult.Location);
												CursorToWorld->SetWorldRotation(CursorR);
										}
					}
					MovementTick(DeltaSeconds);
}

void ATPS_CPPCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
				Super::SetupPlayerInputComponent(NewInputComponent);

				NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPS_CPPCharacter::InputAxisX);
				NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPS_CPPCharacter::InputAxisY);

				/** Camera zoom input*/
				NewInputComponent->BindAxis(TEXT("CameraZoom"), this, &ATPS_CPPCharacter::CameraZoom);

}

void ATPS_CPPCharacter::CameraZoom(const float Value)
{
				if (Value == 0.f || !Controller)
				{
								return;
				}

				const float NewTargetArmLength = CameraBoom->TargetArmLength + Value * ZoomStep;
				CameraBoom->TargetArmLength = FMath::Clamp(NewTargetArmLength, MinZoomLength, MaxZoomLength);
}
void ATPS_CPPCharacter::InputAxisX(float Value)
{
				AxisX = Value;
}

void ATPS_CPPCharacter::InputAxisY(float Value)
{
				AxisY = Value;
}

void ATPS_CPPCharacter::MovementTick(float DeltaTime)
{
				AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
				AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

				APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (myController)
				{
								FHitResult resultHit;
								myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, resultHit);

								float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), resultHit.Location).Yaw;
								SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

				}
}

void ATPS_CPPCharacter::CharacterUpdate()
{
				float ResSpeed = 600.0f;
				switch (MovementState)
				{
				case EMovementState::Aim_State:
								ResSpeed = MovementInfo.AimSpeed;
								break;
				case EMovementState::Walk_State:
								ResSpeed = MovementInfo.WalkSpeed;
								break;
				case EMovementState::Run_State:
								ResSpeed = MovementInfo.RunSpeed;
								break;
				default:
								break;
				}

				GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

}

void ATPS_CPPCharacter::ChangeMovementState(EMovementState NewMovementState)
{
				MovementState = NewMovementState;
				CharacterUpdate();
}

