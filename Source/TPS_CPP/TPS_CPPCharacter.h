// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "TPS_CPPCharacter.generated.h"


UCLASS(Blueprintable)
class ATPS_CPPCharacter : public ACharacter
{
				GENERATED_BODY()
public:
	ATPS_CPPCharacter();
							
				// Called every frame.
				virtual void Tick(float DeltaSeconds) override;
				
				/** Reload Player Inputs bu AxisX&AxisY **/
				virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
				
				/** Returns TopDownCameraComponent subobject **/
				FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
				/** Returns CameraBoom subobject **/
				FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
				/** Returns CursorToWorld subobject **/
				FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }


				/** Camera Zoom*/

				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Zoom")
				float MinZoomLength;
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Zoom")
				float MaxZoomLength;
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Zoom")
				float DefaultArmLength;
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Zoom")
				float ZoomStep;

				void CameraZoom(const float Value);
private:
				/** Top down camera */
				UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
				class UCameraComponent* TopDownCameraComponent;
				
				/** Camera boom positioning the camera above the character */
				UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
				class USpringArmComponent* CameraBoom;
				
				/** A decal that projects to the cursor location. */
				UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
				class UDecalComponent* CursorToWorld;

public:
				/** Variables for */
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
				EMovementState MovementState = EMovementState::Run_State;
				/** Variables for */
				UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
				FCharacterSpeed MovementInfo;
				/** Reload Input. */
				UFUNCTION()
				void InputAxisX(float Value);
				/** Reload Input. */
				UFUNCTION()
				void InputAxisY(float Value);

				float AxisX = 0.0f;
				float AxisY = 0.0f;
				/** Tick Funcion */
				UFUNCTION()
				void MovementTick(float DeltaTime);
				
				UFUNCTION(BlueprintCallable)
				void CharacterUpdate();
				
				UFUNCTION(BlueprintCallable)
				void ChangeMovementState(EMovementState NewMovementState);
};

