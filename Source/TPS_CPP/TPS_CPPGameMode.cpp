// Copyright Epic Games, Inc. All Rights Reserved.

#include "TPS_CPPGameMode.h"
#include "TPS_CPPPlayerController.h"
#include "TPS_CPPCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_CPPGameMode::ATPS_CPPGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_CPPPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/CharacterBP"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
